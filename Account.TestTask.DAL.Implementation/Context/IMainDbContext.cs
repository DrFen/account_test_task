﻿using System.Data.Entity;
using Account.TestTask.DbModels.Account;

namespace Account.TestTask.DAL.Implementation.Context
{
    /// <summary>
    /// Интерфейс контекста БД
    /// </summary>
    public interface IMainDbContext 
    {
        #region properties
        /// <summary>
        /// Набор данных для работы со счётом
        /// </summary>
        DbSet<AccountDb> Accounts { get; set; }

        /// <summary>
        /// Набор данных для работы с историей счёта
        /// </summary>
        DbSet<AccountHistoryDb> AccountHistories { get; set; }
        #endregion properties


        #region methods

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
        #endregion methods

    }
}
