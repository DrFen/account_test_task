﻿using System.Data.Entity;
using Account.TestTask.DbModels.Account;

namespace Account.TestTask.DAL.Implementation.Context
{
    /// <summary>
    /// Контекст работы с БД
    /// </summary>
    public class MainDbContext : DbContext, IMainDbContext
    {
        #region properties
        /// <summary>
        /// Набор данных для работы со счётом
        /// </summary>
        public DbSet<AccountDb> Accounts { get; set; }

        /// <summary>
        /// Набор данных для работы с историей счёта
        /// </summary>
        public DbSet<AccountHistoryDb> AccountHistories { get; set; }

        #endregion properties

        #region constructor

        public MainDbContext() : base("MainConnectionString")
        {

        }
        #endregion constructor


    }
}
