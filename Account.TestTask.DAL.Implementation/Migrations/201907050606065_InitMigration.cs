namespace Account.TestTask.DAL.Implementation.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class InitMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "account.accout_history",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        account_id = c.Int(nullable: false),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        changed = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("account.accout", t => t.account_id, cascadeDelete: true)
                .Index(t => t.account_id, name: "idx_accout_history_account_id");
            
            CreateTable(
                "account.accout",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        account_number = c.String(nullable: false, maxLength: 40),
                        balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        row_version = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.id)
                .Index(t => t.account_number, unique: true, name: "uidx_account_account_number");
            
        }
        
        public override void Down()
        {
            DropForeignKey("account.accout_history", "account_id", "account.accout");
            DropIndex("account.accout", "uidx_account_account_number");
            DropIndex("account.accout_history", "idx_accout_history_account_id");
            DropTable("account.accout");
            DropTable("account.accout_history");
        }
    }
}
