﻿using System;
using System.Data.Entity;
using System.Linq;
using Account.TestTask.DAL.Implementation.Context;
using Account.TestTask.DAL.Interfaces.Common;
using Account.TestTask.DbModels.Common;

namespace Account.TestTask.DAL.Implementation.Common
{
    /// <summary>
    /// Реализация простого репозитория реализующего CRUD
    /// </summary>
    /// <typeparam name="TDbModel"></typeparam>
    public abstract class SimpleRepository<TDbModel> : ISimpleRepository<TDbModel> where TDbModel : class, IEntity<TDbModel>
    {
        #region fields

        /// <summary>
        /// Контекст работы с БД
        /// </summary>
        private readonly IMainDbContext _context;

        private readonly Func<IMainDbContext, IDbSet<TDbModel>> _getDbSet;
        #endregion fields

        #region constructors

        /// <summary>
        /// Конструктор простого репозитория реализующего CRUD сущности
        /// </summary>
        /// <param name="context">Контекст работы с БД</param>
        /// <param name="getDbSet">Получение набора данных контекста</param>
        public SimpleRepository(IMainDbContext context, Func<IMainDbContext, DbSet<TDbModel>> getDbSet)
        {
            _context = context;
            _getDbSet = getDbSet;
        }
        #endregion constructors

        #region methods
        /// <summary>
        /// Получение списка
        /// </summary>
        /// <returns>IQueryable моделей БД</returns>
        public IQueryable<TDbModel> GetList()
        {
            return _getDbSet(_context);
        }


        /// <summary>
        /// Получение записи по идентификатору
        /// </summary>
        /// <param name="id">Идентфикатор сущности</param>
        /// <returns>Модель БД</returns>
        /// <exception cref="InvalidOperationException">Объект не найден</exception>  
        public TDbModel GetById(int id)
        {
            return GetList().Single(s => s.Id == id);
        }

        /// <summary>
        /// Добавление записи
        /// </summary>
        /// <param name="entity">Модель БД</param>
        public int Insert(TDbModel entity)
        {
            _getDbSet(_context).Add(entity);
            _context.SaveChanges();

            return entity.Id;

        }


        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="entity">Модель БД</param>
        /// <exception cref="InvalidOperationException">Объект не найден</exception>  
        public void Update(TDbModel entity)
        {
            if (entity?.Id == null)
                throw new ArgumentNullException();

            var currentEntity = GetById(entity.Id);

            Update(currentEntity, entity);

        }

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="currentEntity">Модель БД</param>
        /// <param name="entity">Модель с обновлёнными данными</param>
        /// <exception cref="InvalidOperationException">Объект не найден</exception>  
        public void Update(TDbModel currentEntity, TDbModel entity)
        {

            currentEntity.OnUpdate(entity);
            _context.SaveChanges();

        }

        #endregion methods

    }
}
