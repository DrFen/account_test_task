﻿using Account.TestTask.DAL.Implementation.Common;
using Account.TestTask.DAL.Implementation.Context;
using Account.TestTask.DbModels.Account;

namespace Account.TestTask.DAL.Implementation.Repositories
{
    /// <summary>
    /// Репозиторий работы с историей по счёту
    /// </summary>
    public class AccountHistoryRepository: SimpleRepository<AccountHistoryDb>
    {
        public AccountHistoryRepository(IMainDbContext context) : base(context, c=>c.AccountHistories)
        {
        }
    }
}
