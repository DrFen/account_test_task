﻿using Account.TestTask.DAL.Implementation.Common;
using Account.TestTask.DAL.Implementation.Context;
using Account.TestTask.DbModels.Account;

namespace Account.TestTask.DAL.Implementation.Repositories
{
    /// <summary>
    /// Репозиторий работы со счетами
    /// </summary>
    public class AccountRepository : SimpleRepository<AccountDb>
    {
        public AccountRepository(IMainDbContext context) : base(context, c=>c.Accounts)
        {
        }
    }
}
