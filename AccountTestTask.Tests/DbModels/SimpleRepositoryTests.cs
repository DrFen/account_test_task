﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AccountTestTask.Tests.DbModels.ForRepositoryTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AccountTestTask.Tests.DbModels
{
    /// <summary>
    /// Тестирование методов репозитория
    /// </summary>
    [TestClass]
    public class SimpleRepositoryTests
    {


        /// <summary>
        /// Получение mock контекста для работы с dummy сущностью
        /// </summary>
        /// <returns></returns>
        private ITestDbContext GetContext()
        {
            var list = new List<DummyDbModel>
            {
                new DummyDbModel
                {
                    Id = 1,
                    Name = "1",
                    InvariableData = "i1"
                },
                new DummyDbModel
                {
                    Id = 2,
                    Name = "2",
                    InvariableData = "i2"
                }
            };

            var historyMock = new Mock<DbSet<DummyDbModel>>();
            historyMock.As<IQueryable<DummyDbModel>>().Setup(m => m.Provider).Returns(list.AsQueryable().Provider);
            historyMock.As<IQueryable<DummyDbModel>>().Setup(m => m.Expression).Returns(list.AsQueryable().Expression);
            historyMock.As<IQueryable<DummyDbModel>>().Setup(m => m.ElementType).Returns(list.AsQueryable().ElementType);
            historyMock.As<IQueryable<DummyDbModel>>().Setup(m => m.GetEnumerator()).Returns(list.AsQueryable().GetEnumerator());
            historyMock.Setup(s => s.Add(It.IsAny<DummyDbModel>()))
                .Callback((DummyDbModel model) => list.Add(model));

        

            var contextMock = new Mock<ITestDbContext>();
            contextMock.Setup(s => s.Dummies).Returns(historyMock.Object);

            return contextMock.Object;
        }


        /// <summary>
        /// Проверка успешного получения списка
        /// </summary>
        [TestMethod]
        public void SuccessGetList()
        {
            var list = new DummyRepository(GetContext()).GetList();
            Assert.AreEqual(2, list.Count());
            Assert.AreEqual("i1", list.Single(s=>s.Id ==1).InvariableData);
        }

        /// <summary>
        /// Проверка успешного получения по идентификатору
        /// </summary>
        [TestMethod]
        public void SuccesGetById()
        {
            Assert.AreEqual("i1", new DummyRepository(GetContext()).GetById(1).InvariableData);
            Assert.AreEqual("i2", new DummyRepository(GetContext()).GetById(2).InvariableData);

        }

        /// <summary>
        /// Проверка вызова исключения при получении несуществующего объекта
        /// </summary>
        [TestMethod]
        public void NotFoundGetByIdTest()
        {
            Assert.ThrowsException<InvalidOperationException>(() => new DummyRepository(GetContext()).GetById(100));
        }

        /// <summary>
        /// Проверка успешного добавления сущности
        /// </summary>
        [TestMethod]
        public void SuccessInsert()
        {
            var repository = new DummyRepository(GetContext());

            repository.Insert(new DummyDbModel
            {
                Id = 3,
                Name = "3",
                InvariableData = "i3"
            });

            Assert.AreEqual("i3", repository.GetById(3).InvariableData);
        }
    }
}
