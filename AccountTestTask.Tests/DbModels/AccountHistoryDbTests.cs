﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Account.TestTask.DAL.Implementation.Context;
using Account.TestTask.DAL.Implementation.Repositories;
using Account.TestTask.DbModels.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AccountTestTask.Tests.DbModels
{
    [TestClass]
    public class AccountHistoryDbTests
    {
        /// <summary>
        /// Проверка, что обновление сущности истории операций по счёту недопустимо
        /// </summary>
        [TestMethod]
        public void UpdateDeniedTest()
        {


            var historyList = new List<AccountHistoryDb>
            {
                new AccountHistoryDb
                {
                    Id = 1,
                    AccountId = 1,
                    Amount = 1,
                    ChangeAt = new DateTime()
                }
            }.AsQueryable();

            var historyMock = new Mock<DbSet<AccountHistoryDb>>();
            historyMock.As<IQueryable<AccountHistoryDb>>().Setup(m => m.Provider).Returns(historyList.Provider);
            historyMock.As<IQueryable<AccountHistoryDb>>().Setup(m => m.Expression).Returns(historyList.Expression);
            historyMock.As<IQueryable<AccountHistoryDb>>().Setup(m => m.ElementType).Returns(historyList.ElementType);
            historyMock.As<IQueryable<AccountHistoryDb>>().Setup(m => m.GetEnumerator()).Returns(historyList.GetEnumerator());

            var contextMock = new Mock<IMainDbContext>();
            contextMock.Setup(s => s.AccountHistories).Returns(historyMock.Object);

            var repository = new AccountHistoryRepository(contextMock.Object);
            Assert.ThrowsException<NotImplementedException>(() => repository.Update(new AccountHistoryDb{Id = 1}));
        }
    }
}
