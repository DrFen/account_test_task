﻿using Account.TestTask.DbModels.Common;

namespace AccountTestTask.Tests.DbModels.ForRepositoryTesting
{
    /// <summary>
    /// Фэйковая сущность для тестирования репозитория
    /// </summary>
    public class DummyDbModel : IEntity<DummyDbModel>
    {
        #region properties
        /// <summary>
        /// Идентфикатор фэйковой сущности
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование фэйковой сущности
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Неизменяемая при обновлении строка фэйковой сущности
        /// </summary>
        public string InvariableData { get; set; }
        #endregion properties

        #region methods

        public void OnUpdate(DummyDbModel updatedEntity)
        {
            Name = updatedEntity.Name;
        }
        #endregion methods

    }
}
