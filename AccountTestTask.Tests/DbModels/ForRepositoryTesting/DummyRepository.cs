﻿using Account.TestTask.DAL.Implementation.Common;

namespace AccountTestTask.Tests.DbModels.ForRepositoryTesting
{
    /// <summary>
    /// Фэйковый репозиторий для тестов
    /// </summary>
     public class DummyRepository : SimpleRepository<DummyDbModel>
    {
        public DummyRepository(ITestDbContext context) : base(context, c => ((ITestDbContext)c).Dummies)
        {

        }
    }
}
