﻿using System.Data.Entity;
using Account.TestTask.DAL.Implementation.Context;

namespace AccountTestTask.Tests.DbModels.ForRepositoryTesting
{
    /// <summary>
    /// Интерфейс для покрытия тестами SimpleReposittory
    /// </summary>
    public interface ITestDbContext : IMainDbContext
    {
        DbSet<DummyDbModel> Dummies { get; set; }
    }
}
