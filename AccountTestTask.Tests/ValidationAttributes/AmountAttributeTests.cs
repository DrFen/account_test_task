﻿using Account.TeskTask.BLL.Models.ValidationAttributes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AccountTestTask.Tests.ValidationAttributes
{
    /// <summary>
    /// Тесты атрибута валидации числового значения
    /// </summary>
    [TestClass]
    public class AmountAttributeTests
    {

        /// <summary>
        /// Проверка валидных данных
        /// </summary>
        [TestMethod]
        public void CheckRightData()
        {
            Assert.IsTrue(new AmountAttribute().IsValid(100));
        }

        /// <summary>
        /// Проверка, если атрибут расположен не на числовом поле
        /// </summary>
        [TestMethod]
        public void CheckNotNumericData()
        {
            Assert.IsFalse(new AmountAttribute().IsValid(new { test = 1 }));
        }

        /// <summary>
        ///  Проверка при значении меньше 0
        /// </summary>
        [TestMethod]
        public void CheckBelowZero()
        {
            Assert.IsFalse(new AmountAttribute().IsValid(-1));
            Assert.IsFalse(new AmountAttribute().IsValid(decimal.MinValue));
        }

        /// <summary>
        /// Проверка при значении ==0
        /// </summary>

        [TestMethod]
        public void CheckEqualZero()
        {
            Assert.IsFalse(new AmountAttribute().IsValid(0));
        }

    }
}
