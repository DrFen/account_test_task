﻿using System;
using System.Collections.Generic;
using System.Linq;
using Account.TestTask.BLL.Implementation.Account;
using Account.TestTask.BLL.Implementation.Exceptions;
using Account.TestTask.DAL.Interfaces.Common;
using Account.TestTask.DbModels.Account;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace AccountTestTask.Tests.Services
{
    [TestClass]
    public class AccountServicesTests
    {

        /// <summary>
        /// Mock репозитория работы со счетами
        /// </summary>
        /// <returns></returns>
        private ISimpleRepository<AccountDb> GetAccountRepository()
        {
            var mockRepository = new Mock<ISimpleRepository<AccountDb>>();

            var data = new Dictionary<int, AccountDb>
            {
                {
                    1, new AccountDb
                    {
                        Id = 1,
                        Balance = 100,
                        AccountNumber = "1"
                    }
                },
                {
                    2, new AccountDb
                    {
                        Id = 2,
                        Balance = 200,
                        AccountNumber = "2"
                    }
                },
                {3, new AccountDb
                    {
                        Id = 3,
                        Balance = decimal.MaxValue - 10,
                        AccountNumber = "3"
                    }
                },
                {
                    4, new AccountDb
                    {
                        Id = 4,
                        Balance = 0,
                        AccountNumber = "4"
                    }
                }
            };

            mockRepository.Setup(s => s.GetList()).Returns(data.Select(s => s.Value).AsQueryable);

            mockRepository.Setup(s => s.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                if (!data.ContainsKey(id))
                    throw new InvalidOperationException();

                return data[id];
            });

            mockRepository.Setup(s => s.Insert(It.IsAny<AccountDb>())).Callback((AccountDb model) =>
            {
                var newId = data.Max(m => m.Key) + 1;
                model.Id = newId;
                data.Add(newId, model);
            });


            mockRepository.Setup(s => s.Update(It.IsAny<AccountDb>())).Callback((AccountDb model) =>
            {

                if (model?.Id == null)
                    throw new ArgumentNullException();

                if (!data.ContainsKey(model.Id))
                    throw new InvalidOperationException();

                model.OnUpdate(data[model.Id]);
                data[model.Id] = model;
            });


            mockRepository.Setup(s => s.Update(It.IsAny<AccountDb>(), It.IsAny<AccountDb>())).Callback((AccountDb currentEntity, AccountDb entity) =>
            {

                if (currentEntity?.Id == null)
                    throw new ArgumentNullException();

                if (!data.ContainsKey(currentEntity.Id))
                    throw new InvalidOperationException();

                currentEntity.OnUpdate(entity);

                data[entity.Id] = currentEntity;
            });



            return mockRepository.Object;
        }

        /// <summary>
        /// Mock репозитория работы с историей операций по счету
        /// </summary>
        /// <returns></returns>
        private ISimpleRepository<AccountHistoryDb> GetAccountHistoryRepository()
        {
            var mockRepository = new Mock<ISimpleRepository<AccountHistoryDb>>();

            var data = new Dictionary<int, AccountHistoryDb>
            {
                {
                    1, new AccountHistoryDb
                    {
                       Id = 1,
                       AccountId = 1,
                       ChangeAt = new DateTime(2019,1,1),
                       Amount = 150
                    }
                },
                {
                    2, new AccountHistoryDb
                    {
                        Id = 2,
                        AccountId = 1,
                        ChangeAt = new DateTime(2019,1,2),
                        Amount = -100
                    }
                },
                {
                    3, new AccountHistoryDb
                    {
                        Id = 3,
                        AccountId = 1,
                        ChangeAt = new DateTime(2019,1,3),
                        Amount = 50
                    }
                },
                {
                    4, new AccountHistoryDb
                    {
                        Id = 4,
                        AccountId = 2,
                        ChangeAt = new DateTime(2019,1,1),
                        Amount = 100
                    }
                },
                {
                    5, new AccountHistoryDb
                    {
                        Id = 5,
                        AccountId = 2,
                        ChangeAt = new DateTime(2019,1,2),
                        Amount = 100
                    }
                },
                {
                    6, new AccountHistoryDb
                    {
                        Id = 6,
                        AccountId = 3,
                        ChangeAt = new DateTime(2019,1,1),
                        Amount = decimal.MaxValue - 10
                    }
                }
            };

            mockRepository.Setup(s => s.GetList()).Returns(data.Select(s => s.Value).AsQueryable);

            mockRepository.Setup(s => s.GetById(It.IsAny<int>())).Returns((int id) =>
            {
                if (!data.ContainsKey(id))
                    throw new InvalidOperationException();

                return data[id];
            });

            mockRepository.Setup(s => s.Insert(It.IsAny<AccountHistoryDb>())).Callback((AccountHistoryDb model) =>
            {
                var newId = data.Max(m => m.Key) + 1;
                model.Id = newId;
                data.Add(newId, model);
            });


            mockRepository.Setup(s => s.Update(It.IsAny<AccountHistoryDb>())).Callback((AccountHistoryDb model) =>
            {

                if (model?.Id == null)
                    throw new ArgumentNullException();

                if (!data.ContainsKey(model.Id))
                    throw new InvalidOperationException();

                data[model.Id] = model;
            });



            return mockRepository.Object;
        }

        private AccountService GetAccountService()
        {
            return new AccountService(GetAccountRepository(), GetAccountHistoryRepository());
        }

        /// <summary>
        /// Проверка успешности получения списка
        /// </summary>
        [TestMethod]
        public void SuccessGetHistoryList()
        {
            var result = GetAccountService().GetHistory(1);
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(new DateTime(2019, 1, 3), result.First().OperationDate);
            Assert.AreEqual(new DateTime(2019, 1, 1), result.Last().OperationDate);
            Assert.AreEqual(0, result.Count(c => c.AccountNumber != "1"));
        }

        /// <summary>
        /// Проверка успешности получения списка счёта с пустой историей
        /// </summary>
        [TestMethod]
        public void GetEmptyHistoryList()
        {
            var result = GetAccountService().GetHistory(4);
            Assert.AreEqual(0, result.Count);
          
        }

        /// <summary>
        /// Проверка вызова исключения при ненахождении счёта
        /// </summary>
        [TestMethod]
        public void NoAccountHistoryList()
        {
            Assert.ThrowsException<NoAccountException>(() => GetAccountService().GetHistory(100));
        }

        /// <summary>
        /// Проверка успешного пополнения
        /// </summary>
        [TestMethod]
        public void SuccessRefill()
        {
            var accountRepository = GetAccountRepository();
            var accountHistoryRepository = GetAccountHistoryRepository();
            var service = new AccountService(accountRepository, accountHistoryRepository);

            var result = service.Refill(1, 1000);
            Assert.AreEqual(1100, result);
            Assert.AreEqual(1100, accountRepository.GetById(1).Balance);
            Assert.AreEqual(1000, accountHistoryRepository.GetList().OrderBy(o => o.Id).Last().Amount);
        }

        /// <summary>
        /// Проверка попытки пополнения на несуществующий счёт
        /// </summary>
        [TestMethod]
        public void NoAccountRefill()
        {
            Assert.ThrowsException<NoAccountException>(() => GetAccountService().Refill(1000, 1000));
        }

        /// <summary>
        /// Проверка попытки пополнения слишком большой суммой
        /// </summary>
        [TestMethod]
        public void AmountIsTooBigRefill()
        {
            Assert.ThrowsException<AmountIsTooBigException>(() => GetAccountService().Refill(1, decimal.MaxValue));
        }

        /// <summary>
        /// Проверка успешного списания со счёта
        /// </summary>
        [TestMethod]
        public void SuccessWithdraw()
        {
            var accountRepository = GetAccountRepository();
            var accountHistoryRepository = GetAccountHistoryRepository();
            var service = new AccountService(accountRepository, accountHistoryRepository);
            var result = service.Withdraw(1, 20);

            Assert.AreEqual(80, accountRepository.GetById(1).Balance);
            Assert.AreEqual(80, result);
            Assert.AreEqual(-20, accountHistoryRepository.GetList().OrderBy(o => o.Id).Last().Amount);
        }

        /// <summary>
        /// Попытка списания с отсутствующего счёта 
        /// </summary>
        [TestMethod]
        public void NoAccountWithdraw()
        {
            Assert.ThrowsException<NoAccountException>(() => GetAccountService().Withdraw(1000, 1000));
        }

        /// <summary>
        /// Попытка списания суммы больше остатка
        /// </summary>
        [TestMethod]
        public void InsufficientlyMoneyWithdraw()
        {
            Assert.ThrowsException<InsufficientlyMoneyException>(() => GetAccountService().Withdraw(1, decimal.MaxValue));
        }


        /// <summary>
        /// Успешный перевод между счетами
        /// </summary>
        [TestMethod]
        public void SuccessTransfer()
        {
            var accountRepository = GetAccountRepository();
            var accountHistoryRepository = GetAccountHistoryRepository();
            var service = new AccountService(accountRepository, accountHistoryRepository);

            var result = service.Transfer(1, 2, 20);

            Assert.AreEqual(80, result.SourceBalance);
            Assert.AreEqual(220, result.DestinationBalance);


            Assert.AreEqual(80, accountRepository.GetById(1).Balance);
            Assert.AreEqual(220, accountRepository.GetById(2).Balance);

            var operations = accountHistoryRepository.GetList().OrderBy(o => o.Id).ToArray();
            Assert.AreEqual(0, operations[operations.Length - 1].Amount + operations[operations.Length - 2].Amount);

        }

        /// <summary>
        /// Попытка перевод с/на несуществующий счёт
        /// </summary>
        /// <param name="fromAccountId">Счёт с которого выполняется списание</param>
        /// <param name="toAccountId">Счёт на который производится зачисление</param>
        [DataTestMethod]
        [DataRow(1, 100)]
        [DataRow(100, 1)]
        [DataRow(101, 100)]
        public void NoAccountTransfer(int fromAccountId, int toAccountId)
        {
            Assert.ThrowsException<NoAccountException>(() => GetAccountService().Transfer(fromAccountId, toAccountId, 100));
        }

        /// <summary>
        /// Попытка перевода слишком большой суммы
        /// </summary>
        [TestMethod]
        public void InsufficientlyMoneyTransfer()
        {
            Assert.ThrowsException<InsufficientlyMoneyException>(() => GetAccountService().Transfer(1, 2, 1000));
        }



        /// <summary>
        /// Попытка перевода слишком большой суммы
        /// </summary>
        [TestMethod]
        public void AmountIsTooBigTransfer()
        {
            Assert.ThrowsException<AmountIsTooBigException>(() => GetAccountService().Transfer(1, 3, 20));
        }

        /// <summary>
        /// Попытка перевода на тот же счёт
        /// </summary>
        [TestMethod]
        public void TransferToSameAccount()
        {
            Assert.ThrowsException<TransferToSameAccountException>(() => GetAccountService().Transfer(1, 1, 20));

        }

    }
}
