﻿using System.Collections.Generic;
using Account.TeskTask.BLL.Models.Account.Responses;

namespace Account.TestTask.BLL.Interfaces.Account
{
    /// <summary>
    /// Интерфейс работы со счётом
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Получение истории по счёту
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <returns>Список операций</returns>
        List<AccountHistoryListItem> GetHistory(int accountId);

        /// <summary>
        /// Пополнение счёта
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Сумма поплонения</param>
        /// <returns>Баланс после операции</returns>
        decimal Refill(int accountId, decimal amount);

        /// <summary>
        /// Списание со счёта
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Сумма списания</param>
        /// <returns>Баланс после операции</returns>
        decimal Withdraw(int accountId, decimal amount);



        /// <summary>
        /// Перевод между счетами
        /// </summary>
        /// <param name="accountIdFrom">Идентификатор счёта с которого происходит списание</param>
        /// <param name="accountIdTo">Идентфтикатор счёта на который происходит полпонение</param>
        /// <param name="amount">Сумма перевода</param>
        /// <returns>Баланс счетов после операции</returns>
        TransferResponse Transfer(int accountIdFrom, int accountIdTo, decimal amount);


        /// <summary>
        /// Добавление 100 счетов с произвольными данными и одной операцией пополнения на сумму баланса
        /// </summary>
        /// <returns>Список операций аналогичный GetHistory</returns> 
        List<AccountHistoryListItem> GenerateRandomData();

    }
}
