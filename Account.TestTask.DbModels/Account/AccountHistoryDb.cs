﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Account.TestTask.DbModels.Common;

namespace Account.TestTask.DbModels.Account
{
    /// <summary>
    /// Модель БД работы с историей счёта
    /// </summary>
    [Table("accout_history", Schema = "account")]
    public class AccountHistoryDb : IEntity<AccountHistoryDb>
    {
        #region properties
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id", TypeName = "int")]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор номера счёта
        /// </summary>
        [Column("account_id")]
        [Index("idx_accout_history_account_id")]
        [ForeignKey(nameof(Account))]
        [Required]
        public int AccountId { get; set;}

        /// <summary>
        /// Объект-счёт
        /// </summary>
        
        public virtual AccountDb Account { get; set; }

        /// <summary>
        /// Сумма операции
        /// </summary>
        [Column("amount")]
        [Required]
        public decimal Amount { get; set; }

        /// <summary>
        /// Дата проведения операции
        /// </summary>
        [Column("changed")]
        [Required]
        public DateTime ChangeAt { get; set; }
        #endregion properties

        #region methods
        /// <summary>
        /// Действия выполняемые при обновлении истории по счёту.
        /// Обновление истории запрещено, всегда будет возвращаться NotImplementedException
        /// </summary>
        /// <param name="updatedEntity">Обновлённые данные по истории</param>
        /// <exception cref="NotImplementedException">Ожидаемый результат независимо от данных</exception>  
        public void OnUpdate(AccountHistoryDb updatedEntity)
        {
            throw new NotImplementedException();
        }
        #endregion methods


    }
}
