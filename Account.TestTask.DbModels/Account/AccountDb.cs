﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Account.TestTask.DbModels.Common;

namespace Account.TestTask.DbModels.Account
{
    /// <summary>
    /// Модель БД работы со счётом
    /// </summary>
    [Table("accout", Schema = "account")]
    public class AccountDb : IEntity<AccountDb>
    {
        #region properties
        /// <summary>
        /// Иеднфтикатор счёта
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id", TypeName = "int")]
        public int Id { get; set; }


        /// <summary>
        /// Номер счёта 
        /// </summary>
        [Column("account_number")]
        [Index("uidx_account_account_number", IsUnique = true)]
        [MaxLength(40)]
        [Required]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Баланс
        /// </summary>
        [Column("balance")]
        [Required]
        public decimal Balance { get; set; }

        /// <summary>
        /// Реализация оптимистичной блокировки
        /// </summary>
        [Timestamp]
        [Column("row_version")]
        public byte[] RowVersion { get; set; }

        #endregion properties

        #region methods

        /// <summary>
        /// Действия выполняемые при обновлении сущности. Разрешено обновлять только баланс счёта.
        /// Собственно данные счёта считаем неизменными из-за отсутствия API
        /// Примем, что данные приходя к нам из сторонней системы
        /// </summary>
        /// <param name="updatedEntity">Новые данные по счёту</param>
        public void OnUpdate(AccountDb updatedEntity)
        {
            Balance = updatedEntity.Balance;
        }
        #endregion methods

    }
}
