﻿namespace Account.TestTask.DbModels.Common
{
    public interface IEntity<in TDbModel> where TDbModel : class, IEntity<TDbModel>
    {
        /// <summary>
        /// Идентфиикатор сущности
        /// </summary>
        int Id { get; set;}

        /// <summary>
        /// Действия выполняемые при обновлении сущности
        /// </summary>
        /// <param name="updatedEntity">Сущность на которую необходимо обновить, модель БД</param>
        void OnUpdate(TDbModel updatedEntity);
    }
}
