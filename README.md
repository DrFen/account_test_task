# Тестовое задание

## Исходные данные
В качестве исходных данных использована постановка см. task.pdf

## Подключение и настройка решения

Для начала работы необходимо:
* Указать стартовым проектом Account.TestTask.Api
* Указать строку подключения к БД в файле Account.TestTask.Api/Web.config
* Развернуть БД одним из указанных ниже способов

### С помощью sql скрипта
На целевой БД необходимо выполнить SQL скрипт **Account.TestTask.DAL.Implementation/Migrations/SQL/InitMigration.sql**

### С помощью package manager console
В Visual Studio в package manager console неоюходимо выполнить команду **Update-Database -ProjectName "Account.TestTask.DAL.Implementation"** и дождаться её выполнения

### Используемые языки, пакеты и технологии
* Все проекты написаны на языке C# 7.3 с использованием .net framework 4.5.2
* В качестве СУБД для отладки использовался MS SQL Server Express 16
* В качестве ORM использован EntityFramework 6
* В качестве IoC контейнера используется Microsoft Unity
* В качестве framework для тестирования используется MSTest





