﻿using Newtonsoft.Json;

namespace Account.TeskTask.BLL.Models.Account.Responses
{
    /// <summary>
    /// Результат выполнения перемещения денег между счетами
    /// </summary>
    public class TransferResponse
    {
        /// <summary>
        /// Баланс исходного счёта 
        /// </summary>
        [JsonProperty("source_balance")]
        public decimal SourceBalance { get; set; }

        /// <summary>
        /// Баланс целевого счёта
        /// </summary>
        [JsonProperty("destination_balance")]
        public decimal DestinationBalance { get; set; }
    }
}
