﻿using System;
using Newtonsoft.Json;

namespace Account.TeskTask.BLL.Models.Account.Responses
{
    /// <summary>
    /// Элемент списка истории операций по счёту
    /// </summary>
    public class AccountHistoryListItem
    {
        /// <summary>
        /// Номер счёта
        /// </summary>
        [JsonProperty("account_number")]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Дата проведения операции
        /// </summary>
        [JsonProperty("operation_date")]
        public DateTime OperationDate { get; set; }

        /// <summary>
        /// Сумма операции
        /// </summary>
        [JsonProperty("amount")]
        public decimal Amount { get; set; }
    }
}
