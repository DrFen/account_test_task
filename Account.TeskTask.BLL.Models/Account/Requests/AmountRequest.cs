﻿using System.ComponentModel.DataAnnotations;
using Account.TeskTask.BLL.Models.ValidationAttributes;
using Newtonsoft.Json;

namespace Account.TeskTask.BLL.Models.Account.Requests
{
    /// <summary>
    /// Сумма операции
    /// </summary>
    public class AmountRequest
    {
        [JsonProperty("amount")]
        [Required]
        [Amount]
        public decimal Amount { get; set; }
    }
}
