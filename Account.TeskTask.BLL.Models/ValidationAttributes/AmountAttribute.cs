﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Account.TeskTask.BLL.Models.ValidationAttributes
{
    /// <summary>
    /// Атрибут проверки на соответствие сумме платежа
    /// </summary>
    [AttributeUsage(AttributeTargets.Property |AttributeTargets.Field, AllowMultiple = false)]
    public class AmountAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {

            if (!decimal.TryParse(value.ToString(), out var decimalObject))
            {
                ErrorMessage = "It`s not a decimal value";
                return false;
            }

            ErrorMessage = "Число должно быть больше 0";
            return decimalObject > 0;

        }
    }
}
