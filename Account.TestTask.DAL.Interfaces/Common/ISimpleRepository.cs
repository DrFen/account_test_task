﻿using System;
using System.Linq;
using Account.TestTask.DbModels.Common;

namespace Account.TestTask.DAL.Interfaces.Common
{

    /// <summary>
    /// Интерфейс простого репозитория реализующего CRUD
    /// </summary>
    /// <typeparam name="TDbModel"></typeparam>
    public interface ISimpleRepository<TDbModel> where TDbModel : class, IEntity<TDbModel>
    {
        /// <summary>
        /// Получение списка
        /// </summary>
        /// <returns>IQueryable моделей БД</returns>
        IQueryable<TDbModel> GetList();

        /// <summary>
        /// Получение записи по идентификатору
        /// </summary>
        /// <param name="id">Идентфикатор сущности</param>
        /// <returns>Модель БД</returns>
        TDbModel GetById(int id);

        /// <summary>
        /// Добавление записи
        /// </summary>
        /// <param name="entity">Модель БД</param>
        int Insert(TDbModel entity);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="entity">Модель БД</param>
        /// <exception cref="InvalidOperationException">Объект не найден</exception>  
        void Update(TDbModel entity);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="currentEntity">Модель БД</param>
        /// <param name="entity">Модель с обновлёнными данными</param>
        /// <exception cref="InvalidOperationException">Объект не найден</exception>  
        void Update(TDbModel currentEntity, TDbModel entity);


    }
}
