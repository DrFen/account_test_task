﻿using System;

namespace Account.TestTask.BLL.Implementation.Exceptions
{
    /// <summary>
    /// Ошибка. Не найден номер счёта
    /// </summary>
    public class NoAccountException : Exception
    {
        public NoAccountException(int id) : base($"Счёт с идентикатором = '{id} не найден'") { }
    }
}
