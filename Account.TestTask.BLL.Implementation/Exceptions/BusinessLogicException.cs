﻿using System;

namespace Account.TestTask.BLL.Implementation.Exceptions
{
    /// <summary>
    /// Общий класс всех ошиое бизнес-логики
    /// </summary>
    public abstract class BusinessLogicException : Exception
    {
        public BusinessLogicException(string message) : base(message) { }
    }
}
