﻿namespace Account.TestTask.BLL.Implementation.Exceptions
{
    /// <summary>
    /// Ошибка. Попытка выполнения перевода на тот же счёт
    /// </summary>
    public class TransferToSameAccountException : BusinessLogicException
    {
        public TransferToSameAccountException() : base("Попытка выполнения перевода на тот же счёт") { }
    }
}
