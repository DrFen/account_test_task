﻿namespace Account.TestTask.BLL.Implementation.Exceptions
{
    /// <summary>
    /// Превышен размер суммы
    /// </summary>
    public class AmountIsTooBigException : BusinessLogicException
    {
        public  AmountIsTooBigException(): base("Сумма слишком большая") { }
    }
}
