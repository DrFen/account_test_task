﻿namespace Account.TestTask.BLL.Implementation.Exceptions
{
    /// <summary>
    /// Ошибка. Сумма недостаточна для проведения операции
    /// </summary>
    public class InsufficientlyMoneyException: BusinessLogicException
    {
        public InsufficientlyMoneyException(decimal amount): base($"Недостаточно денег на счёте, не хватает {amount}") { }
    }
}
