﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Account.TeskTask.BLL.Models.Account.Responses;
using Account.TestTask.BLL.Implementation.Exceptions;
using Account.TestTask.BLL.Interfaces.Account;
using Account.TestTask.DAL.Interfaces.Common;
using Account.TestTask.DbModels.Account;

namespace Account.TestTask.BLL.Implementation.Account
{
    /// <summary>
    /// Сервис работы со счетами
    /// </summary>
    public class AccountService : IAccountService
    {

        #region fields
        /// <summary>
        /// Репозиторий работы со счетами
        /// </summary>
        private readonly ISimpleRepository<AccountDb> _accountRepository;

        /// <summary>
        /// Репозиторий работы с историей счетов
        /// </summary>
        private readonly ISimpleRepository<AccountHistoryDb> _accountHistoryRepository;
        #endregion fields

        #region constructors
        /// <summary>
        /// Сервис работы со счетами
        /// </summary>
        /// <param name="accountRepository">Репозиторий работы со счетами</param>
        /// <param name="accountHistoryRepository">Репозиторий работы с историей счетов</param>
        public AccountService(ISimpleRepository<AccountDb> accountRepository,
                              ISimpleRepository<AccountHistoryDb> accountHistoryRepository)
        {
            _accountRepository = accountRepository;
            _accountHistoryRepository = accountHistoryRepository;
        }
        #endregion constructors

        #region methods
        /// <summary>
        /// Получение истории по счёту
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <returns>Список операций</returns>
        /// <exception cref="NoAccountException">Отсутствует счёт</exception>  
        public List<AccountHistoryListItem> GetHistory(int accountId)
        {
            var result = _accountHistoryRepository.GetList()
                .Where(w => w.AccountId == accountId)
                .Join(_accountRepository.GetList(),
                    h => h.AccountId,
                    a => a.Id,
                    (h, a) => new AccountHistoryListItem
                    {
                        AccountNumber = a.AccountNumber,
                        Amount = h.Amount,
                        OperationDate = h.ChangeAt
                    })
                .OrderByDescending(o => o.OperationDate)
                .ToList();

            if (result.Any())
                return result;

            if (!_accountRepository.GetList().Any(a => a.Id == accountId))
                throw new NoAccountException(accountId);

            return result;
        }



        /// <summary>
        /// Изменение баланса счёта
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Количество для изменения</param>
        /// <returns></returns>
        private decimal ChangeBalance(int accountId, decimal amount)
        {
            try
            {
                var account = _accountRepository.GetById(accountId);

                CheckAmount(account.Balance, amount);


                _accountRepository.Update(account, new AccountDb
                {
                    Balance = account.Balance + amount,
                    Id = accountId,
                    AccountNumber = account.AccountNumber
                });

                _accountHistoryRepository.Insert(new AccountHistoryDb
                {
                    AccountId = accountId,
                    Amount = amount,
                    ChangeAt = DateTime.Now
                });

                return account.Balance;
            }
            catch (InvalidOperationException)
            {
                throw new NoAccountException(accountId);
            }
        }

        /// <summary>
        /// Проверка на соответствие критериям суммы
        /// </summary>
        /// <param name="accountBalance">Текущий баланс счёта</param>
        /// <param name="operationAmount">Сумма операции</param>
        private void CheckAmount(decimal accountBalance, decimal operationAmount)
        {
            if (operationAmount > 0 && decimal.MaxValue - accountBalance < operationAmount)
                throw new AmountIsTooBigException();

            var balanceAfter = accountBalance + operationAmount;

            if (balanceAfter < 0)
                throw new InsufficientlyMoneyException(balanceAfter * -1);

        }

        /// <summary>
        /// Пополнение счёта
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Сумма поплонения</param>
        /// <returns>Баланс после операции</returns>
        /// <exception cref="NoAccountException">Отсутствует счёт</exception>  
        /// <exception cref="AmountIsTooBigException">Слишком большая сумма, переполнение переменной</exception>  
        public decimal Refill(int accountId, decimal amount)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    var result = ChangeBalance(accountId, amount);
                    transaction.Complete();
                    return result;
                }
                catch
                {
                    transaction.Dispose();
                    throw;
                }
            }
        }

        /// <summary>
        /// Списание со счёта
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Сумма списания</param>
        /// <returns>Баланс после операции</returns>
        /// <exception cref="NoAccountException">Отсутствует счёт</exception>  
        /// <exception cref="InsufficientlyMoneyException">Недостаточно денег на счёте</exception>  
        public decimal Withdraw(int accountId, decimal amount)
        {
            using (var transaction = new TransactionScope())
            {
                try
                {
                    var result = ChangeBalance(accountId, amount * -1);
                    transaction.Complete();
                    return result;
                }
                catch
                {
                    transaction.Dispose();
                    throw;
                }
            }
        }

        /// <summary>
        /// Перевод между счетами
        /// </summary>
        /// <param name="accountIdFrom">Идентификатор счёта с которого происходит списание</param>
        /// <param name="accountIdTo">Идентфтикатор счёта на который происходит полпонение</param>
        /// <param name="amount">Сумма перевода</param>
        /// <returns>Баланс счетов после операции</returns>
        /// <exception cref="NoAccountException">Отсутствует счёт</exception>  
        /// <exception cref="InsufficientlyMoneyException">Недостаточно денег на счёте</exception>  
        /// <exception cref="AmountIsTooBigException">Слишком большая сумма, переполнение переменной</exception>  
        /// <exception cref="TransferToSameAccountException">Попытка выполнения перевода на тот же счёт</exception>  
        public TransferResponse Transfer(int accountIdFrom, int accountIdTo, decimal amount)
        {
            if (accountIdFrom == accountIdTo)
                throw new TransferToSameAccountException();

            using (var transaction = new TransactionScope())
            {
                try
                {
                    var result = new TransferResponse
                    {
                        SourceBalance = ChangeBalance(accountIdFrom, amount * -1),
                        DestinationBalance = ChangeBalance(accountIdTo, amount * 1)
                    };

                    transaction.Complete();
                    return result;
                }
                catch
                {
                    transaction.Dispose();
                    throw;
                }
            }
        }

        /// <summary>
        /// Добавление 100 счетов с произвольными данными и одной операцией пополнения на сумму баланса
        /// </summary>
        /// <returns>Список операций аналогичный GetHistory</returns> 
        public List<AccountHistoryListItem> GenerateRandomData()
        {
            var result = new List<AccountHistoryListItem>();

            var random = new Random();
            using (var transaction = new TransactionScope())
            {
                try
                {
                    for (var i = 0; i < 100; i++)
                    {
                        var newAccount = new AccountHistoryListItem
                        {
                            AccountNumber = Guid.NewGuid().ToString(),
                            Amount = random.Next(0, 100),
                            OperationDate = DateTime.Now
                        };

                        var accountId =_accountRepository.Insert(new AccountDb
                        {
                            AccountNumber = newAccount.AccountNumber,
                            Balance = newAccount.Amount
                        });

                        _accountHistoryRepository.Insert(new AccountHistoryDb
                        {
                            AccountId = accountId,
                            Amount = newAccount.Amount,
                            ChangeAt = newAccount.OperationDate
                        });

                        result.Add(newAccount);
                    }

                    transaction.Complete();
                }
                catch
                {
                    transaction.Dispose();
                    throw;
                }
            }
            
          



            return result;
        }

        #endregion methods

    }
}
