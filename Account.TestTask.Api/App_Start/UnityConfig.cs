using Account.TestTask.BLL.Implementation.Account;
using Account.TestTask.BLL.Interfaces.Account;
using Account.TestTask.DAL.Implementation.Context;
using Account.TestTask.DAL.Implementation.Repositories;
using Account.TestTask.DAL.Interfaces.Common;
using Account.TestTask.DbModels.Account;
using System;

using Unity;
using Unity.AspNet.Mvc;

namespace Account.TestTask.Api
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            #region contexts
            container.RegisterType<IMainDbContext, MainDbContext>(new PerRequestLifetimeManager());
            #endregion contexts

            #region services
            container.RegisterType<IAccountService, AccountService>();
            #endregion services

            #region repositories

            container.RegisterType<ISimpleRepository<AccountDb>, AccountRepository>();
            container.RegisterType<ISimpleRepository<AccountHistoryDb>, AccountHistoryRepository>();

            #endregion repositories
        }
    }
}