﻿using System;
using System.Linq;
using System.Web.Http;
using Account.TestTask.BLL.Implementation.Exceptions;

namespace Account.TestTask.Api.Common
{
    /// <summary>
    /// Общий контроллер
    /// </summary>
    public class DefaultController : ApiController
    {
        /// <summary>
        /// Обёртка вызова действия контроллеров
        /// </summary>
        /// <typeparam name="TActionResult">Результат выполнения сервиса</typeparam>
        /// <param name="action">Вызов сервиса</param>
        /// <returns></returns>
        protected internal IHttpActionResult InvokeAction<TActionResult>(Func<TActionResult> action)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(s => s.Errors)
                    .Select(s => s.ErrorMessage)
                    .Aggregate((f, s) => $"{f} \n {s}");

                return BadRequest(errors);
            }

            try
            {
                return Ok(DataWrapper<TActionResult>.Success(action()));
            }
            catch (NoAccountException)
            {
                return NotFound();
            }
            catch (BusinessLogicException ex)
            {
                return Ok(DataWrapper<string>.BusinessError(ex.Message));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
