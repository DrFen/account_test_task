﻿using System;
using Newtonsoft.Json;

namespace Account.TestTask.Api.Common
{
    /// <summary>
    /// Класс обёртка данных для передачи клиенту
    /// </summary>
    public class DataWrapper<TResultType>
    {
        #region constants

        /// <summary>
        /// Код ответа при успехе
        /// </summary>
        private static string SuccessResult = "ok";

        /// <summary>
        /// Код ответа при успехе
        /// </summary>
        private static string BusinessLogicError = "error";

        #endregion constants

        #region fields
        /// <summary>
        /// Статус ответа на запрос
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; private set; }

        /// <summary>
        /// Результат выполнения запроса
        /// </summary>
        [JsonProperty("result")]
        public TResultType Result { get; private set; }
        #endregion fields

        #region constructor
        /// <summary>
        /// Oбёртка данных для передачи клиенту
        /// </summary>
        /// <param name="status">Статус запроса</param>
        /// <param name="result">Результат запроса</param>
        public DataWrapper(string status, TResultType result)
        {
            if(status!= SuccessResult && status!=BusinessLogicError)
                throw new NotImplementedException();

            Status = status;
            Result = result;
        }
        #endregion constructor

        #region static methods

        /// <summary>
        /// Успешный ответ на запрос
        /// </summary>
        /// <param name="result">Результат запроса</param>
        /// <returns></returns>
        public static DataWrapper<TResultType> Success(TResultType result)
        {
            return new DataWrapper<TResultType>(SuccessResult, result);
        }

        /// <summary>
        /// Бизнес-правило вызвало исключение
        /// </summary>
        /// <param name="result">Описание ошибки</param>
        /// <returns></returns>
        public static DataWrapper<TResultType> BusinessError(TResultType result)
        {
            return new DataWrapper<TResultType>(BusinessLogicError, result);
        }
        #endregion static methods

    }
}