﻿using System.Web.Http;
using Account.TeskTask.BLL.Models.Account.Requests;
using Account.TestTask.Api.Common;
using Account.TestTask.BLL.Interfaces.Account;

namespace Account.TestTask.Api.Controllers
{
    /// <summary>
    /// Контроллер для работы со счетами
    /// </summary>
    [RoutePrefix("api/account")]
    public class AccountController : DefaultController
    {
        #region fields

        /// <summary>
        /// Сервис работы со счетами
        /// </summary>
        private readonly IAccountService _accountService;

        #endregion fields

        #region constructors
        /// <summary>
        /// Контроллер работы со счетами
        /// </summary>
        /// <param name="accountService">Сервис работы со счетами</param>
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        #endregion constructors

        #region methods

        /// <summary>
        /// Добавление 100 счетов с произвольными данными и одной операцией пополнения на сумму баланса
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("random-data")]
        public IHttpActionResult FillRandomData()
        {
            return InvokeAction(() => _accountService.GenerateRandomData());
        }


        /// <summary>
        /// Получения полного списка операций по счету 
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{accountId:int}/history")]
        public IHttpActionResult History(int accountId)
        {
            return InvokeAction(() => _accountService.GetHistory(accountId));
        }


        /// <summary>
        /// Внесение денег на счет
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Данные по сумме опреации</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{accountId:int}/top-up")]
        public IHttpActionResult Refill(int accountId, AmountRequest amount)
        {
            return InvokeAction(() => _accountService.Refill(accountId, amount.Amount));
        }

        /// <summary>
        /// Снятие денег со счета 
        /// </summary>
        /// <param name="accountId">Идентификатор счёта</param>
        /// <param name="amount">Данные по сумме опреации</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{accountId:int}/withdraw")]
        public IHttpActionResult Withdraw(int accountId, AmountRequest amount)
        {
            return InvokeAction(() => _accountService.Withdraw(accountId, amount.Amount));
        }

        /// <summary>
        /// Перемещение денег между счетами 
        /// </summary>
        /// <param name="sourceAccountId">Идентификатор исходного счёта</param>
        /// <param name="destinationAccountId">Идентфикатор счёта получателя</param>
        /// <param name="amount">Данные по сумме опреации</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{sourceAccountId:int}/transfer/{destinationAccountId:int}")]
        public IHttpActionResult Transfer(int sourceAccountId, int destinationAccountId, AmountRequest amount)
        {
            return InvokeAction(() => _accountService.Transfer(sourceAccountId, destinationAccountId, amount.Amount));
        }

        #endregion methods

    }
}
